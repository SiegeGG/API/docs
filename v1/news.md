# Enterprise Endpoint: News

- [Overview](#section-1)
- [Endpoint](#section-2)
- [Example Response](#section-3)

<a name="section-1"></a>

## Overview

This endpoint responds with the latest 10 news articles from SiegeGG in English.

You can change the language with the `language` parameter, such as
`https://api.siege.gg/v1/news?language=de` for German articles.

> Requires scope `news` to be granted.

<a name="section-2"></a>

## Endpoint

`GET https://api.siege.gg/v1/news`

<a name="section-3"></a>

## Example Response

```json
{
  "data": [
    {
      "id": 1,
      "author": "Hunter Cooke",
      "url": "https://siege.gg/news/1",
      "url_with_slug": "https://siege.gg/news/1-test-article",
      "image": "https://siege.gg/img/articles/test-article.png",
      "image_cdn": "https://cdn.siege.gg/img/articles/test-article.png",
      "language": "en",
      "title": "Test Article",
      "hook": "An amazing test article you have to read!",
      "tags": [
        "pro-league",
        "EG",
        "G2"
      ],
      "created_at": "2019-01-01T01:01:00.000Z"
    },
    ... (and 9 more)
  ],
  "pagination": null
}
```

This endpoint does not support pagination. Therefore the `pagination` variable (see [the pagination explaination](/v1/pagination.md)) will be `null` in any case.
