# Pagination

- [What is Pagination?](#section-1)
- [Pagination in the API](#section-2)
- [Contentual Explaination](#section-3)

<a name="section-1"></a>

## What is Pagination?

On the web, pagination is commonly refered to as wrapping existing data into a page based view. SiegeGG uses this technique on multiple pages, most notably on the [SiegeGG news page](https://siege.gg/news).

<a name="section-2"></a>

## Pagination in the API

In the API we use pagination to divide the big amount of data into multiple requests. Each data response from the API will be structured like the following example. However, if the endpoint does not support pagination, the `pagination` object will be `null`.

```json
{
    "data": [
        {
            "name": "Chanka",
            "favorite_operator": "TACHANKA"
        },
        ... and more
    ],
    "pagination": {
        "page": 0,
        "count": 99,
        "total": 9
    }
}
```

<a name="section-3"></a>

## Contentual Explaination

| Key     | Explaination                               |
| ------- | ------------------------------------------ |
| `page`  | Current page (starts at 0)                 |
| `count` | Count of all available entries<sup>1</sup> |
| `total` | Total amount of pages                      |

---

<sup>1</sup> This may differ from application to application as they may have varying scopes.
