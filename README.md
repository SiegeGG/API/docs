# Docs for SiegeGG API

Welcome to the official SiegeGG API documentation. This repository is
primarily a helpful resource for developers who want to use the SiegeGG
API in their projects.

## Contributing

Even though the SiegeGG API is a proprietary system, we welcome all
contributions to our open source libraries and this documentation
repository.

## Accessing the API

**Note**: the `v1` API has been internally deprecated on 2021-08-11 and
has reached end-of-life on 2021-12-17. The `/news` endpoint will be
available during an extended support period until the `v2` API is
available.

The API‘s endpoints are available at `https://api.siege.gg`. At the
moment, the only API version is `v1`. All endpoints are accessible
under the `/v1` path. The url, `https://api.siege.gg/v1/news`, is an
example fetching SiegeGG articles.

The API is using strict HTTPS and will redirect all HTTP requests to
HTTPS. Always implement requests to the SiegeGG API as HTTPS requests to
ensure that no unencrypted data is transmitted.

## API Applications

Our API is only on a small scale free-to-use. In order to authorize requests that have the permission to fetch specific data, we use so called API applications. An application is directly linked to an API token which is used to authorize with the API. All requests have to supply a token using the HTTP `Authorization` header.

Suppose your API key is `ICL_iUL5j_Nu-.bZ`, the authorization header would be: `Authorization: ICL_iUL5j_Nu-.bZ`.

You can create API applications in the [SiegeGG Developer Hub](https://siege.gg/dev).

## Application Plans

Each API application has one of three plans. A plan defines the maximum number of requests that can be performed every 30 minutes.

| Name           | Quota   | Details                                                                                                |
| -------------- | ------- | ------------------------------------------------------------------------------------------------------ |
| **community**  | `100`   | The **community** plan can be used free of charge but attribution is required.                         |
| **staff**      | `500`   | Any staff member can create a **staff** application free of charge.                                    |
| **enterprise** | `1000+` | [Contact us](mailto:contact@siege.gg) to get access to more data endpoints and a higher request quota. |

---

![image](https://staff-cdn.siege.gg/01c5deab5d5d054bc15ce5577ed033bd/siegegg-twitter-banner-2020.jpeg)

&mdash; SiegeGG
